//
//  AboutViewController.swift


import UIKit

class AboutViewController: UIViewController {

    @IBOutlet weak var aboutText: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()
      self.setText()
      var navBarColor = navigationController!.navigationBar
      navBarColor.barTintColor = GlobalObjects.colorTheme
      navBarColor.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor()]

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backAboutPressed(sender: AnyObject) {
        performSegueWithIdentifier("About Segue", sender: self)
    }
    func setText(){
    navigationController?.navigationBar.topItem?.title = "About Us".localized()
     
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
