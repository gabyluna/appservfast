//
//  FavouritesViewController.swift

//

import UIKit
import WatsonDeveloperCloud

class FavouritesViewController: UITableViewController{
  

 var databasePath = NSString()
    var player: AVAudioPlayer?
  var namePlaceFavourite: String = ""
  var latitudeFavourite: String = ""
  var longitudeFavourite: String = ""
  var placeIdFavourite: String = ""
  var openFavourite: String = ""
  var addressPlaceFavourite: String!
  var dataArray:[FavouritePlaceData] = []
  var indexOfSelectedPerson = 0
  
  let data : NSMutableArray = NSMutableArray()
    override func viewDidLoad() {
        super.viewDidLoad()
        createDataBase()
      
       findFavouritePlaces()
      var navBarColor = navigationController!.navigationBar
      navBarColor.barTintColor = GlobalObjects.colorTheme
      navBarColor.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor()]
      self.setText()
  }

    @IBAction func seePlacePressed(sender: AnyObject) {
    }
  
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
   
    
    if(segue.identifier == "Favourite Place Segue") {
      let controller: PlacesViewController = segue.destinationViewController as! PlacesViewController
      controller.name = self.namePlaceFavourite
      controller.address = self.addressPlaceFavourite
      controller.latitudePlace = self.latitudeFavourite
      controller.longitudePlace = self.longitudeFavourite
      controller.open = self.openFavourite
      controller.longitudeCurrent = GlobalObjects.longitude
      controller.latitudeCurrent = GlobalObjects.latitude

    }

  }
  
  override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
  
  func findFavouritePlaces(){
    let contactDB = FMDatabase(path: databasePath as String)
    
    if contactDB.open() {
      let querySQL = "SELECT * FROM FAVOURITES"
      
      let resultSet: FMResultSet! = contactDB.executeQuery(querySQL,  withArgumentsInArray: [])
     
     
      
      while (resultSet!.next() == true) {
        let place_id = resultSet?.stringForColumn("place_id")
        let name = resultSet?.stringForColumn("name")
        let address = resultSet?.stringForColumn("address")
        let longitude = resultSet?.stringForColumn("longitude")
        let latitude = resultSet?.stringForColumn("latitude")
        let open = resultSet?.stringForColumn("open")
      
        let multiField = FavouritePlaceData(aField1: place_id!, aField2: name!, aField3: address!, aField4: longitude!, aField5: latitude!, aField6: open!)
        self.dataArray.append(multiField)
        data.addObject(name!)
        
      }
      
    } else {
      print("Error: \(contactDB.lastErrorMessage())")
    }
     contactDB.close()
  
  }
  
  func createDataBase(){
    
    let filemgr = NSFileManager.defaultManager()
    let dirPaths =
    NSSearchPathForDirectoriesInDomains(.DocumentDirectory,
      .UserDomainMask, true)
    
    let docsDir = dirPaths[0] 
    
    databasePath = docsDir.stringByAppendingString("/favouritePlaces.db")
    
    if !filemgr.fileExistsAtPath(databasePath as String) {
      
      let favouriteDB = FMDatabase(path: databasePath as String)
      
      if favouriteDB == nil {
        print("Error: \(favouriteDB.lastErrorMessage())")
      }
      
      if favouriteDB.open() {
        let sql_stmt = "CREATE TABLE IF NOT EXISTS FAVOURITES (ID INTEGER PRIMARY KEY AUTOINCREMENT, PLACE_ID TEXT, NAME TEXT, ADDRESS TEXT, LONGITUDE TEXT, LATITUDE TEXT,OPEN TEXT)"
        if !favouriteDB.executeStatements(sql_stmt) {
          print("Error: \(favouriteDB.lastErrorMessage())")
        }
        favouriteDB.close()
      } else {
        print("Error: \(favouriteDB.lastErrorMessage())")
      }
    }
    
  }

    @IBAction func backFavPressed(sender: AnyObject) {
      
        performSegueWithIdentifier("Favourite Segue", sender: self)
    }
  
  

  
  override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return dataArray.count
  }
 
  override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    
    
    tableView.deselectRowAtIndexPath(indexPath, animated: true)
    indexOfSelectedPerson = indexPath.row
    
    placeIdFavourite = dataArray[indexOfSelectedPerson].place_id as! String
    namePlaceFavourite = dataArray[indexOfSelectedPerson].name as! String
    addressPlaceFavourite = dataArray[indexOfSelectedPerson].address as! String
    longitudeFavourite = dataArray[indexOfSelectedPerson].longitude as! String
    latitudeFavourite = dataArray[indexOfSelectedPerson].latitude as! String
    openFavourite = dataArray[indexOfSelectedPerson].open as! String
    self.performSegueWithIdentifier("Favourite Place Segue", sender: self)
    print(indexOfSelectedPerson)
    let service = TextToSpeech(username: "4a4380da-fd9e-4743-bfca-691645acf8b0", password: "hRE5PGYt534f")
    service.synthesize(namePlaceFavourite,voice:"es-US_SofiaVoice", completionHandler: {
      data, error in
      if let audio = data {
        
        do {
          self.player = try AVAudioPlayer(data: audio)
          self.player!.play()
        } catch {
          print("Couldn't create player.")
        }
        
      } else {
        print(error)
      }
      }
    )
    
  
    
    
  }
  
  override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    
    //let cell = tableView.dequeueReusableCellWithIdentifier("favouritePlace", forIndexPath: indexPath)
     let cell: UITableViewCell = UITableViewCell(style: UITableViewCellStyle.Subtitle, reuseIdentifier: "favouritePlace")
    let multiField: FavouritePlaceData = self.dataArray[indexPath.row]
    
    let num = indexPath.row + 1
    
    cell.textLabel?.text = "\(num). \(multiField.name!)"
    cell.detailTextLabel?.text = multiField.address as? String
    
    
    //cell.textLabel?.text = data[indexPath.row] as! String
    return cell
  }
  func setText(){
    navigationController?.navigationBar.topItem?.title = "Favourite Places".localized()
  }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
