//
//  ThemeViewController.swift
//  Feed Me
//
//  Created by administrator on 10/1/16.
//  Copyright © 2016 Ron Kliffer. All rights reserved.
//

import UIKit
import WatsonDeveloperCloud

class ThemeViewController: UITableViewController {
  
  var dataArray: [String] = ["Black", "Gray", "Red"]
    var player: AVAudioPlayer?
  override func viewDidLoad() {
    super.viewDidLoad()
    
    var navBarColor = navigationController!.navigationBar
    navBarColor.barTintColor = GlobalObjects.colorTheme
    navBarColor.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor()]
    
    
  }
  
  
    @IBAction func backPressed(sender: AnyObject) {
        performSegueWithIdentifier("Theme Segue", sender: self)
    }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return dataArray.count
  }
  
  override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    let service = TextToSpeech(username: "4a4380da-fd9e-4743-bfca-691645acf8b0", password: "hRE5PGYt534f")
    service.synthesize("Theme changed", completionHandler: {
      data, error in
      if let audio = data {
        
        do {
          self.player = try AVAudioPlayer(data: audio)
          self.player!.play()
        } catch {
          print("Couldn't create player.")
        }
        
      } else {
        print(error)
      }
      }
    )
    tableView.deselectRowAtIndexPath(indexPath, animated: true)
    indexPath.row
    var color = dataArray[indexPath.row] as! String
    
    var colorSelect:UIColor = UIColor()
    if(color == "Black"){
      colorSelect = UIColor.blackColor()
    }
    if(color == "Gray"){
      colorSelect = UIColor.grayColor()

    }
    if(color == "Red"){
      colorSelect = UIColor.redColor()

    }
    
    GlobalObjects.colorTheme = colorSelect
    self.performSegueWithIdentifier("Theme Segue", sender: self)


    
  }
  
  override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    
    let cell: UITableViewCell = UITableViewCell(style: UITableViewCellStyle.Subtitle, reuseIdentifier: "themeCell")
    let multiField: String = self.dataArray[indexPath.row]
    
    
    cell.textLabel?.text = "\(multiField)"
    cell.imageView!.image = UIImage(named: multiField)
  
    return cell
  }

}
