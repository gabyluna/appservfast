//
//  MapViewController.swift


import UIKit
import WatsonDeveloperCloud

class MapViewController: UIViewController {
   var player: AVAudioPlayer?
  let service = TextToSpeech(username: "4a4380da-fd9e-4743-bfca-691645acf8b0", password: "hRE5PGYt534f")
  @IBOutlet weak var mapView: GMSMapView!
  @IBOutlet weak var mapCenterPinImage: UIImageView!
  @IBOutlet weak var addressLabel: UILabel!
  @IBOutlet weak var pinImageVerticalConstraint: NSLayoutConstraint!
  var searchedTypes = ["bank","beauty_salon","food","gas_station","gym","health","laundry", "motoring", "police","shopping_mall","bus_station","airport"]
  let locationManager = CLLocationManager()
  let dataProvider = GoogleDataProvider()
  var placesClient: GMSPlacesClient?
  let gpaViewController = GooglePlacesAutocomplete(
  apiKey: GlobalObjects.keyApiGoogle,
  placeType: .Establishment
  )
  
  
  let searchRadius: Double = 1000
  var namePlace: String = ""
  static var latitudeCurrent: String = ""
  static var longitudeCurrent: String = ""
  var latitudePlaceS: String = ""
  var longitudePlaceS: String = ""
  var placeIdPlace: String = ""
 
  var namePlaceSearch: String = ""
  var latitudeSearch: String = ""
  var longitudeSearch: String = ""
  var addressPlaceSearch: String = ""
  var openSearh: String = "true"
  
  
  var openNow: String = ""
  override func viewDidLoad() {
    super.viewDidLoad()
    
    locationManager.delegate = self
    locationManager.requestWhenInUseAuthorization()
    locationManager.requestAlwaysAuthorization()
    mapView.delegate = self
    
    gpaViewController.placeDelegate = self
    
    var navBarColor = navigationController!.navigationBar
    navBarColor.barTintColor = GlobalObjects.colorTheme
    navBarColor.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor()]
    
  }
  
  
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    if segue.identifier == "Types Segue" {
      let navigationController = segue.destinationViewController as! UINavigationController
      let controller = navigationController.topViewController as! TypesTableViewController
      controller.selectedTypes = searchedTypes
      controller.delegate = self
    }
    if segue.identifier == "Places Segue" {
      self.service.synthesize(self.namePlace,voice:"es-US_SofiaVoice", completionHandler: {
        data, error in
        if let audio = data {
          
          do {
            self.player = try AVAudioPlayer(data: audio)
            self.player!.play()
          } catch {
            print("Couldn't create player.")
          }
          
        } else {
          print(error)
        }
        }
      )
      let navigationController = segue.destinationViewController as! UINavigationController
      let controller = navigationController.topViewController as! PlacesViewController
      controller.open = self.openNow
      controller.address = self.addressLabel.text  as String!
      controller.latitudeCurrent = GlobalObjects.latitude
      controller.longitudeCurrent = GlobalObjects.longitude
      controller.longitudePlace = self.longitudePlaceS
      controller.latitudePlace = self.latitudePlaceS
      controller.placeIDPlace = self.placeIdPlace
      controller.name = self.namePlace
      
    }
    
    if(segue.identifier == "Search Place Segue") {
      let controllerSearch: PlacesViewController = segue.destinationViewController as! PlacesViewController
      controllerSearch.name = self.namePlaceSearch
      controllerSearch.address = self.addressPlaceSearch
      controllerSearch.latitudePlace = self.latitudeSearch
      controllerSearch.longitudePlace = self.longitudeSearch
      controllerSearch.open = self.openSearh
      controllerSearch.longitudeCurrent = GlobalObjects.longitude
      controllerSearch.latitudeCurrent = GlobalObjects.latitude
      
    }
  }
    
    @IBAction func searchPressed(sender: AnyObject) {
        
        presentViewController(gpaViewController, animated: true, completion: nil)
    }
    
  
  func reverseGeocodeCoordinate(coordinate: CLLocationCoordinate2D) {
    let geocoder = GMSGeocoder()
   
        geocoder.reverseGeocodeCoordinate(coordinate) { response , error in
      self.addressLabel.unlock()
      if let address = response?.firstResult() {
        let lines = address.lines as! [String]
        self.addressLabel.text = lines.joinWithSeparator("\n")
        self.latitudePlaceS = String(coordinate.latitude)
        self.longitudePlaceS = String(coordinate.longitude)
        
        let labelHeight = self.addressLabel.intrinsicContentSize().height
        self.mapView.padding = UIEdgeInsets(top: self.topLayoutGuide.length, left: 0, bottom: labelHeight, right: 0)
        
        UIView.animateWithDuration(0.25) {
          self.pinImageVerticalConstraint.constant = ((labelHeight - self.topLayoutGuide.length) * 0.5)
          self.view.layoutIfNeeded()
        }
      }
      
     
    }
  }
  
  func fetchNearbyPlaces(coordinate: CLLocationCoordinate2D) {
    mapView.clear()
    dataProvider.fetchPlacesNearCoordinate(coordinate, radius:searchRadius, types: searchedTypes) { places in
      for place: GooglePlace in places {
        let marker = PlaceMarker(place: place)
        marker.map = self.mapView
       
      
      }
    }
  }
    
    @IBOutlet weak var searchButton: UIBarButtonItem!
  @IBAction func refreshPlaces(sender: AnyObject) {
    fetchNearbyPlaces(mapView.camera.target)
   
  }
  
}

// MARK: - TypesTableViewControllerDelegate
extension MapViewController: TypesTableViewControllerDelegate {
  func typesController(controller: TypesTableViewController, didSelectTypes types: [String]) {
    searchedTypes = controller.selectedTypes.sort()
    dismissViewControllerAnimated(true, completion: nil)
    fetchNearbyPlaces(mapView.camera.target)
  }
}

// MARK: - CLLocationManagerDelegate
extension MapViewController: CLLocationManagerDelegate {
  func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
    if status == .AuthorizedWhenInUse {
      locationManager.startUpdatingLocation()
      mapView.myLocationEnabled = true
      mapView.settings.myLocationButton = true
      
    }
  }
  
  

  func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
    if let location = locations.first {
      mapView.camera = GMSCameraPosition(target: location.coordinate, zoom: 15, bearing: 0, viewingAngle: 0)
      locationManager.stopUpdatingLocation()
      GlobalObjects.latitude = String(location.coordinate.latitude)
      GlobalObjects.longitude = String (location.coordinate.longitude)
      fetchNearbyPlaces(location.coordinate)
    }
  }
}

// MARK: - GMSMapViewDelegate
extension MapViewController: GMSMapViewDelegate {
  func mapView(mapView: GMSMapView!, idleAtCameraPosition position: GMSCameraPosition!) {
    reverseGeocodeCoordinate(position.target)
  }
  
  func mapView(mapView: GMSMapView!, willMove gesture: Bool) {
    addressLabel.lock()
    
    if (gesture) {
      mapCenterPinImage.fadeIn(0.25)
      mapView.selectedMarker = nil
    }
  }
  
  func mapView(mapView: GMSMapView!, markerInfoContents marker: GMSMarker!) -> UIView! {
    let placeMarker = marker as! PlaceMarker
    
    if let infoView = UIView.viewFromNibName("MarkerInfoView") as? MarkerInfoView {
      infoView.nameLabel.text = placeMarker.place.name
      
      self.namePlace = placeMarker.place.name
      service.synthesize(self.namePlace,voice:"es-US_SofiaVoice", completionHandler: {
        data, error in
        if let audio = data {
          
          do {
            self.player = try AVAudioPlayer(data: audio)
            self.player!.play()
          } catch {
            print("Couldn't create player.")
          }
          
        } else {
          print(error)
        }
        }
      )
      
      self.openNow = placeMarker.place.openNow!
      self.placeIdPlace = placeMarker.place.placeid!

      
      if let photo = placeMarker.place.photo {
        infoView.placePhoto.image = photo
      } else {
        infoView.placePhoto.image = UIImage(named: "generic")
      }
      
      return infoView
    } else {
      return nil
    }
  }
  
  func mapView(mapView: GMSMapView!, didTapMarker marker: GMSMarker!) -> Bool {
    mapCenterPinImage.fadeOut(0.25)
    return false
  }
  
  func didTapMyLocationButtonForMapView(mapView: GMSMapView!) -> Bool {
    mapCenterPinImage.fadeIn(0.25)
    mapView.selectedMarker = nil
    return false
  }
}
extension MapViewController: GooglePlacesAutocompleteDelegate {
  func placeSelected(place: Place) {
    //print("descriprion: " + place.description)
 
    place.getDetails { details in
      print(details)

      self.addressPlaceSearch = details.address
      self.namePlaceSearch = details.name
      self.latitudeSearch = String(details.latitude)
      self.longitudeSearch = String(details.longitude)
      self.performSegueWithIdentifier("Search Place Segue", sender: self)
      self.placeViewClosed()
      self.service.synthesize(self.namePlaceSearch,voice:"es-US_SofiaVoice", completionHandler: {
        data, error in
        if let audio = data {
          
          do {
            self.player = try AVAudioPlayer(data: audio)
            self.player!.play()
          } catch {
            print("Couldn't create player.")
          }
          
        } else {
          print(error)
        }
        }
      )
    }
  }
  
  func placeViewClosed() {
    dismissViewControllerAnimated(true, completion: nil)
  }
}