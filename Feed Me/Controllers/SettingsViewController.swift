//
//  SettingsViewController.swift

//

import UIKit
import Localize_Swift

class SettingsViewController: UIViewController {
  
    @IBOutlet weak var changeLanguage: UIButton!
    
    @IBOutlet weak var changeThemes: UIButton!
    @IBOutlet weak var aboutUs: UIButton!
  var actionSheet: UIAlertController!
  
  let availableLanguages = Localize.availableLanguages()

    override func viewDidLoad() {
      super.viewDidLoad()
      var navBarColor = navigationController!.navigationBar
      navBarColor.barTintColor = GlobalObjects.colorTheme
      navBarColor.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor()]
      self.setText()
        // Do any additional setup after loading the view.
    }
  override func viewWillAppear(animated: Bool) {
    super.viewWillAppear(animated)
    NSNotificationCenter.defaultCenter().addObserver(self, selector: "setText", name: LCLLanguageChangeNotification, object: nil)
    navigationController?.navigationBar.topItem?.title = "Settings".localized()
  }
  func setText(){
    changeLanguage.setTitle("Language".localized(), forState: UIControlState.Normal)
    changeThemes.setTitle("Themes".localized(), forState: UIControlState.Normal)
    aboutUs.setTitle("About Us".localized(), forState: UIControlState.Normal)
    navigationController?.navigationBar.topItem?.title = "Settings".localized()
  }

    @IBAction func languagePressed(sender: AnyObject) {
      actionSheet = UIAlertController(title: nil, message: "Switch Language", preferredStyle: UIAlertControllerStyle.ActionSheet) 
      for language in availableLanguages {
        let displayName = Localize.displayNameForLanguage(language)
        let languageAction = UIAlertAction(title: displayName, style: .Default, handler: {
          (alert: UIAlertAction!) -> Void in
          Localize.setCurrentLanguage(language)
        })
        actionSheet.addAction(languageAction)
      }
      let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel, handler: {
        (alert: UIAlertAction) -> Void in
      })
      actionSheet.addAction(cancelAction)
      self.presentViewController(actionSheet, animated: true, completion: nil)
      
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backSettingPressed(sender: AnyObject) {
        performSegueWithIdentifier("Setting Segue", sender: self)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
