//
//  PlacesViewController.swift


import UIKit
import MapKit
import WatsonDeveloperCloud
class PlacesViewController: UIViewController,UITextFieldDelegate,MKMapViewDelegate,UITableViewDataSource,UITableViewDelegate,CLLocationManagerDelegate{
    
    
    @IBOutlet weak var directionsPlace: UITableView!
    @IBOutlet weak var mapView: MKMapView!
  @IBOutlet weak var namePlace: UILabel!
  @IBOutlet weak var addressPlace: UILabel!
  @IBOutlet weak var openNow: UILabel!
  
  var name: String!
  var address: String!
  var open: String!
  var longitudeCurrent: String!
  var latitudeCurrent: String!
  var longitudePlace: String!
  var latitudePlace: String!
  var placeIDPlace: String!
  var player: AVAudioPlayer?
  var tableData = NSArray()
  var mapManager = MapManager()
  var locationManager: CLLocationManager!
  var databasePath = NSString()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    var navBarColor = navigationController!.navigationBar
    navBarColor.barTintColor = GlobalObjects.colorTheme
    navBarColor.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor()]
    
    self.namePlace.text = name
    self.addressPlace.text = address
    if(open == "false") {
      self.openNow.text = "Close now"
    }else{
      self.openNow.text = "Open now"
    }
    
    createDataBase()
    self.mapView?.delegate = self
    self.mapView!.showsUserLocation = true
    usingGoogle()
  }
    
  @IBAction func backMapPressed(sender: AnyObject) {
  
    performSegueWithIdentifier("Map Segue", sender: self)
    }
  
  func createDataBase(){
    
    let filemgr = NSFileManager.defaultManager()
    let dirPaths =
    NSSearchPathForDirectoriesInDomains(.DocumentDirectory,
      .UserDomainMask, true)
    
    let docsDir = dirPaths[0] 
    
    databasePath = docsDir.stringByAppendingString("/favouritePlaces.db")
    
    if !filemgr.fileExistsAtPath(databasePath as String) {
      
      let favouriteDB = FMDatabase(path: databasePath as String)
    
      
      if favouriteDB == nil {
        print("Error1: \(favouriteDB.lastErrorMessage())")
      }
      
      if favouriteDB.open() {
        let sql_stmt = "CREATE TABLE IF NOT EXISTS FAVOURITES (ID INTEGER PRIMARY KEY AUTOINCREMENT, PLACE_ID TEXT, NAME TEXT, ADDRESS TEXT, LONGITUDE TEXT, LATITUDE TEXT,OPEN TEXT)"
        
        print(sql_stmt)
        if !favouriteDB.executeStatements(sql_stmt) {
          print("Error2: \(favouriteDB.lastErrorMessage())")
        }
        favouriteDB.close()
      } else {
        print("Error3: \(favouriteDB.lastErrorMessage())")
      }
    }
    
  }
    @IBAction func addFavouritePlace(sender: AnyObject) {
      let service = TextToSpeech(username: "4a4380da-fd9e-4743-bfca-691645acf8b0", password: "hRE5PGYt534f")
      
      print(databasePath)
      let favouriteDB = FMDatabase(path: databasePath as String)
      
      if favouriteDB.open() {
        
        var openTemp: String!
        if(open == "false") {
          openTemp = "Close now"
        }else{
          openTemp = "Open now"
        }
        
        let insertSQL = "INSERT INTO FAVOURITES (place_id, name, address, longitude, latitude, open) VALUES ('\(placeIDPlace)', '\(name)', '\(address)','\(longitudePlace)','\(latitudePlace)','\(openTemp)')"
        
        let result = favouriteDB.executeUpdate(insertSQL,
          withArgumentsInArray: nil)
        
        if !result {
          service.synthesize("Place not added", completionHandler: {
            data, error in
            if let audio = data {
              
              do {
                self.player = try AVAudioPlayer(data: audio)
                self.player!.play()
              } catch {
                print("Couldn't create player.")
              }
              
            } else {
              print(error)
            }
            }
            )
          print("Failed to add place")
          print("Error4: \(favouriteDB.lastErrorMessage())")
        } else {
          
          let alertController = UIAlertController(title: "Notification", message:
            "Place Added!", preferredStyle: UIAlertControllerStyle.Alert)
          alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default,handler: nil))
          print(insertSQL)
          service.synthesize("Place Added", completionHandler: {
            data, error in
            if let audio = data {
              
              do {
                self.player = try AVAudioPlayer(data: audio)
                self.player!.play()
              } catch {
                print("Couldn't create player.")
              }
              
            } else {
              print(error)
            }
            }
          )
          
          self.presentViewController(alertController, animated: true, completion: nil)
         
        }
      } else {
        print("Error5: \(favouriteDB.lastErrorMessage())")
      }
      
    }

  
  func mapViewWillStartLocatingUser(mapView: MKMapView) {
  }
  
  func mapView(mapView: MKMapView, rendererForOverlay overlay: MKOverlay) -> MKOverlayRenderer {
    if overlay is MKPolyline {
      let polylineRenderer = MKPolylineRenderer(overlay: overlay)
      polylineRenderer.strokeColor = UIColor.blueColor()
      polylineRenderer.lineWidth = 5
      print("done")
      return polylineRenderer
    }
    return MKOverlayRenderer()
  }
  
  func numberOfSectionsInTableView(tableView: UITableView) -> Int{
    return 1
  }
  
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
    return tableData.count
  }
  
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell{
    let cell: UITableViewCell = UITableViewCell(style: UITableViewCellStyle.Subtitle, reuseIdentifier: "direction")
    
    let idx:Int = indexPath.row
    
    let dictTable:NSDictionary = tableData[idx] as! NSDictionary
    let instruction = dictTable["instructions"] as! String
    let distance = dictTable["distance"] as! NSString
    let duration = dictTable["duration"] as! NSString
    let detail = "distance:\(distance) duration:\(duration)"
    
    
    cell.textLabel!.text = instruction
    cell.backgroundColor = UIColor.clearColor()
    cell.textLabel!.font = UIFont(name: "Helvetica Neue Light", size: 15.0)
    cell.selectionStyle = UITableViewCellSelectionStyle.None
    
    //cell.textLabel.font=  [UIFont fontWithName:"Helvetica Neue-Light" size:15];
    cell.detailTextLabel!.text = detail
    
    return cell
  }
  
   func usingGoogle(){
    print(latitudeCurrent)
    print(longitudeCurrent)
   
      
    var origin = latitudeCurrent + ", " + longitudeCurrent
    
    var destination =  latitudePlace + ", " + longitudePlace
    origin = origin.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
    destination = destination.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
    
 
    
    self.view.endEditing(true)
    
    mapManager.directionsUsingGoogle(from: origin, to: destination) { (route,directionInformation, boundingRegion, error) -> () in
      
      if(error != nil){
        print(error)
      }
      else{
        let pointOfOrigin = MKPointAnnotation()
        pointOfOrigin.coordinate = route!.coordinate
        pointOfOrigin.title = directionInformation?.objectForKey("start_address") as! NSString as String
        pointOfOrigin.subtitle = directionInformation?.objectForKey("duration") as! NSString as String
        
        let pointOfDestination = MKPointAnnotation()
        pointOfDestination.coordinate = route!.coordinate
        pointOfDestination.title = directionInformation?.objectForKey("end_address") as! NSString as String
        pointOfDestination.subtitle = directionInformation?.objectForKey("distance") as! NSString as String
        
        let start_location = directionInformation?.objectForKey("start_location") as! NSDictionary
        let originLat = start_location.objectForKey("lat")?.doubleValue
        let originLng = start_location.objectForKey("lng")?.doubleValue
        
        let end_location = directionInformation?.objectForKey("end_location") as! NSDictionary
        let destLat = end_location.objectForKey("lat")?.doubleValue
        let destLng = end_location.objectForKey("lng")?.doubleValue
        
        let coordOrigin = CLLocationCoordinate2D(latitude: originLat!, longitude: originLng!)
        let coordDesitination = CLLocationCoordinate2D(latitude: destLat!, longitude: destLng!)
        
        pointOfOrigin.coordinate = coordOrigin
        pointOfDestination.coordinate = coordDesitination
        if let web = self.mapView {
          dispatch_async(dispatch_get_main_queue()) {
            self.removeAllPlacemarkFromMap(shouldRemoveUserLocation: true)
            web.addOverlay(route!)
            web.addAnnotation(pointOfOrigin)
            web.addAnnotation(pointOfDestination)
            web.setVisibleMapRect(boundingRegion!, animated: true)
            self.directionsPlace?.delegate = self
            self.directionsPlace?.dataSource = self
            self.tableData = directionInformation?.objectForKey("steps") as! NSArray
            self.directionsPlace?.reloadData()
          }
        }
      }
    }
  }
  
  func locationManager(manager: CLLocationManager,
    didChangeAuthorizationStatus status: CLAuthorizationStatus) {
      var hasAuthorised = false
      var locationStatus:NSString = ""
      switch status {
      case CLAuthorizationStatus.Restricted:
        locationStatus = "Restricted Access"
      case CLAuthorizationStatus.Denied:
        locationStatus = "Denied access"
      case CLAuthorizationStatus.NotDetermined:
        locationStatus = "Not determined"
      default:
        locationStatus = "Allowed access"
        hasAuthorised = true
      }
      
     
  }
  
  func removeAllPlacemarkFromMap(shouldRemoveUserLocation shouldRemoveUserLocation:Bool){
    if let mapView = self.mapView {
      for annotation in mapView.annotations{
        if shouldRemoveUserLocation {
          if annotation as? MKUserLocation !=  mapView.userLocation {
            mapView.removeAnnotation(annotation as MKAnnotation)
          }
        }
      }
    }
  }
}


