//
//  AppDelegate.swift


import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
  
  var window: UIWindow?
  // 1
  let googleMapsApiKey = GlobalObjects.keyApiGoogle
  
  func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
    // 2
    GMSServices.provideAPIKey(googleMapsApiKey)
   
   

    return true
  }
  
  func languageWillChange(notification:NSNotification){
    let targetLang = notification.object as! String
    NSUserDefaults.standardUserDefaults().setObject(targetLang, forKey: "selectedLanguage")
    NSBundle.setLanguage(targetLang)
    NSNotificationCenter.defaultCenter().postNotificationName("LANGUAGE_DID_CHANGE", object: targetLang)
  }
}

