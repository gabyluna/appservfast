//
//  GoogleDataProvider.swift


import UIKit
import Foundation
import CoreLocation
import SwiftyJSON


class GoogleDataProvider {
  var photoCache = [String:UIImage]()
  var placesTask: NSURLSessionDataTask?
  var session: NSURLSession {
    return NSURLSession.sharedSession()
  }
  
  let googleMapsApiKey = GlobalObjects.keyApiGoogle
  
  // Return IP address of WiFi interface (en0) as a String, or `nil`
  func getWiFiAddress() -> String? {
    var address : String?
    
    // Get list of all interfaces on the local machine:
    var ifaddr : UnsafeMutablePointer<ifaddrs> = nil
    if getifaddrs(&ifaddr) == 0 {
      
      // For each interface ...
      for (var ptr = ifaddr; ptr != nil; ptr = ptr.memory.ifa_next) {
        let interface = ptr.memory
        
        // Check for IPv4 or IPv6 interface:
        let addrFamily = interface.ifa_addr.memory.sa_family
        if addrFamily == UInt8(AF_INET) || addrFamily == UInt8(AF_INET6) {
          
          // Check interface name:
          if let name = String.fromCString(interface.ifa_name) where name == "en0" {
            
            // Convert interface address to a human readable string:
            var addr = interface.ifa_addr.memory
            var hostname = [CChar](count: Int(NI_MAXHOST), repeatedValue: 0)
            getnameinfo(&addr, socklen_t(interface.ifa_addr.memory.sa_len),
              &hostname, socklen_t(hostname.count),
              nil, socklen_t(0), NI_NUMERICHOST)
            address = String.fromCString(hostname)
          }
        }
      }
      freeifaddrs(ifaddr)
    }
    
    return address
  }
  
  

  
  func fetchPlacesNearCoordinate(coordinate: CLLocationCoordinate2D, radius: Double, types:[String], completion: (([GooglePlace]) -> Void)) -> ()
  {
    
    var urlString = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=\(coordinate.latitude),\(coordinate.longitude)&radius=\(radius)&rankby=prominence&sensor=true"
    let typesString = types.count > 0 ? types.joinWithSeparator("|") : ""
    urlString += "&types=\(typesString)"
    urlString = urlString.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())!
    
    if let task = placesTask where task.taskIdentifier > 0 && task.state == .Running {
      task.cancel()
    }

  
    urlString +=  "&key=" + googleMapsApiKey
   
    UIApplication.sharedApplication().networkActivityIndicatorVisible = true
    placesTask = session.dataTaskWithURL(NSURL(string: urlString)!) {data, response, error in
      UIApplication.sharedApplication().networkActivityIndicatorVisible = false
      var placesArray = [GooglePlace]()
      if let aData = data {
        let json = JSON(data:aData, options:NSJSONReadingOptions.MutableContainers, error:nil)
        if let results = json["results"].arrayObject as? [[String : AnyObject]] {
          for rawPlace in results {
            let place = GooglePlace(dictionary: rawPlace, acceptedTypes: types)
            placesArray.append(place)
            if let reference = place.photoReference {
              self.fetchPhotoFromReference(reference) { image in
                place.photo = image
              }
            }
          }
        }
      }
      dispatch_async(dispatch_get_main_queue()) {
        completion(placesArray)
      }
    }
    placesTask?.resume()
  }
  
  
  func fetchPhotoFromReference(reference: String, completion: ((UIImage?) -> Void)) -> () {
    if let photo = photoCache[reference] as UIImage? {
      completion(photo)
    } else {
      
    
      var urlString = "https://maps.googleapis.com/maps/api/place/photo?maxwidth=200&photoreference=\(reference)"
      
      urlString +=  "&key=" + googleMapsApiKey
      
      UIApplication.sharedApplication().networkActivityIndicatorVisible = true
      session.downloadTaskWithURL(NSURL(string: urlString)!) {url, response, error in
        UIApplication.sharedApplication().networkActivityIndicatorVisible = false
        if let url = url {
          let downloadedPhoto = UIImage(data: NSData(contentsOfURL: url)!)
          self.photoCache[reference] = downloadedPhoto
          dispatch_async(dispatch_get_main_queue()) {
            completion(downloadedPhoto)
          }
        }
        else {
          dispatch_async(dispatch_get_main_queue()) {
            completion(nil)
          }
        }
      }.resume()
    }
  }
}
