//
//  GooglePlace.swift


import UIKit
import Foundation
import CoreLocation
import SwiftyJSON

class GooglePlace {
  let name: String
  let address: String
  let coordinate: CLLocationCoordinate2D
  let placeType: String
  var photoReference: String?
  var photo: UIImage?
  var openNow: String?
  var placeid: String?
  
  init(dictionary:[String : AnyObject], acceptedTypes: [String])
  {
    let json = JSON(dictionary)
    placeid = json["place_id"].stringValue
    name = json["name"].stringValue
    address = json["vicinity"].stringValue
    openNow = json["opening_hours"]["open_now"].stringValue
    
    let lat = json["geometry"]["location"]["lat"].doubleValue as CLLocationDegrees
    let lng = json["geometry"]["location"]["lng"].doubleValue as CLLocationDegrees
    coordinate = CLLocationCoordinate2DMake(lat, lng)
    
    photoReference = json["photos"][0]["photo_reference"].string
    
    var foundType = ""
    let possibleTypes = acceptedTypes.count > 0 ? acceptedTypes : ["bakery", "bar", "cafe", "grocery_or_supermarket", "restaurant","food","accounting","airport","bank","beauty_salon","bus_station","car_dealer","car_rental","car_repair","car_wash","dentist","doctor","finance","fire_station","gas_station","hair_care","health","hospital","laundry","parking","pharmacy","physiotherapist","police","restaurant","shopping_mall","spa","taxi_stand","train_station"
    ]

    for type in json["types"].arrayObject as! [String] {
      if possibleTypes.contains(type) {
        foundType = type
        break
      }
    }
    placeType = foundType
  }
}
