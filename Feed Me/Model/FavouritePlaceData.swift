//
//  FavouritePlaceData.swift


import Foundation
class FavouritePlaceData: NSObject {
  var place_id: NSString?
  var name: NSString?
  var address: NSString?
  var longitude: NSString?
  var latitude: NSString?
  var open: NSString?
  

  
  override init() {
    super.init()
  }
  
  convenience init(aField1:NSString, aField2:NSString,aField3:NSString, aField4:NSString, aField5:NSString, aField6:NSString) {
    self.init()
  
      self.place_id = aField1
      self.name = aField2
      self.address = aField3
      self.longitude = aField4
      self.latitude = aField5
      self.open = aField6
    
    
  }
  
}
